var restify = require('restify');
var server = restify.createServer();
server.use(restify.plugins.bodyParser());
server.use(restify.plugins.queryParser());
var request = require('request');
var JSONInfo = require('./moduleJSON');

var port_number = server.listen(process.env.PORT || 3000);
server.listen(port_number,function(){
   console.log('Server Activation');
   
   var returnValue = " ";
   var firebase = require("firebase");
   var config = {
                    apiKey: "AIzaSyBjPs19_HsIW8EQ-d8Iqa_MFT-qfheIS5g",
                    authDomain: "wild-doctor.firebaseapp.com",
                    databaseURL: "https://wild-doctor.firebaseio.com",
                    projectId: "wild-doctor",
                    storageBucket: "wild-doctor.appspot.com",
                    messagingSenderId: "23522572045"
                };
    firebase.initializeApp(config);
    var db = firebase.database();
    
   server.post("/",function(req,res,next){
       console.log('Got Post request');
       var name = req.body.name;
       var description = req.body.description;
       var ref = db.ref("/Test/"+name);
       var value = {
        Name: name,
        Description: description
       }
       ref.set(value);
       var jsonInfo = {
           "State" : "Done",
           "Message" : name + " is upload to database."
       };
       console.log(jsonInfo);
       var jsonInfoStr = JSON.stringify(jsonInfo);
       res.send(jsonInfoStr);
       res.end();
   });
   
   server.get("/*",function(req,res,next){
       console.log('Got Get request');
       var name = req.query.name;
       var ref = db.ref("/Test/" + name);
       ref.once("value", function(snapshot) {
       if (snapshot.val() != null) {
           console.log(snapshot.val());
           var string = JSON.stringify(snapshot.val());
           var objectValue = JSON.parse(string);
           var JSONString = JSONInfo(objectValue['Name'],objectValue['Description']);
           var jsonInfo = {
               "State" : "Done",
               "Message" : JSONString
           };
           console.log(jsonInfo);
           var jsonInfoStr = JSON.stringify(jsonInfo);
           res.send(jsonInfoStr);
           res.end();
       } else {
           request('https://en.wikipedia.org/w/api.php?action=parse&section=0&prop=text&page='+name+'&format=json', function (error, response, body) {
                            var json = JSON.parse(body);
                            if (!("error" in json)){
                                var JSONString = JSONInfo(name,json['parse']['text']['*']);
                                var jsonInfo = {
                                    "State" : "Done",
                                    "Description" : JSONString
                                };
                                console.log(jsonInfo);
                                var jsonInfoStr = JSON.stringify(jsonInfo);
                                res.send(jsonInfoStr);
                                res.end();
                                var ref = db.ref("/Test/"+name);
                                var value = {
                                    Name: name,
                                    Description: json['parse']['text']['*']
                                }
                                ref.set(value);
                            }});
       } 
       });
    });
   
   server.del("/*",function(req,res,next){
       console.log('Got Delete request');
       var name = req.query.name;
       var ref = db.ref("/Test/" + name);
       ref.once("value", function(snapshot) {
       if (snapshot.val() != null) {
           db.ref("/Test/" + name).remove()
           var jsonInfo = {
               "State" : "Done",
               "Message" : name + " is deleted."
           };
           console.log(jsonInfo);
           var jsonInfoStr = JSON.stringify(jsonInfo);
           res.send(jsonInfoStr);
           res.end();
       } else {
           var jsonInfo = {
                "State" : "Fair",
                "Message" : name + " can't delete because it is not on database."
           };
           console.log(jsonInfo);
           var jsonInfoStr = JSON.stringify(jsonInfo);
           res.send(jsonInfoStr);
           res.end();
        }
       });
    });
});